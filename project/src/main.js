import Vue from 'vue'
import App from './App.vue'
import router from './router'
// validation
import Vuelidate from 'vuelidate'
// font awesome 
import { library } from '@fortawesome/fontawesome-svg-core'
import { faSort, faSortUp, faSortDown, faExclamationTriangle } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'

// validation
Vue.use(Vuelidate)
// font awesome 
library.add(faSort, faSortUp, faSortDown, faExclamationTriangle)
Vue.component('font-awesome-icon', FontAwesomeIcon)
Vue.config.productionTip = false
Vue.config.productionTip = false

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
