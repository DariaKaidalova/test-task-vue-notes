export default {

  addNote(newNote, notesList) {
    if(newNote.id === '') {
      const firstId = notesList[0] ? notesList[0].id : 0;
      const nextId = notesList.reduce((max, next) => Math.max(max, next.id), firstId) + 1;
      newNote.id = nextId;
    }
    notesList.push(newNote);
    return notesList;
  },

  sortNotesList(sortColumn, notesList, isAsc) {
    notesList.sort((currentNote, nextNote) => {
      if(currentNote[sortColumn] < nextNote[sortColumn]) {
        return isAsc ? 1 : -1;
      }
      if(currentNote[sortColumn] > nextNote[sortColumn]) {
        return isAsc ? -1 : 1;
      }
      return 0;
    });
    return notesList;
  },

  deleteNotes(deleteNotesArray, notesList) {
    return notesList.filter(note => !deleteNotesArray.includes(note.id));
  },

  calculateStatistics(notesList) {
    const statistics = {
      total: {
        title: "Total", 
        value: 0
      },
      completed: {
        title: "Completed", 
        value: 0
      },
      notCompleted: {
        title: "Not completed",
        value : 0
      }
    };
    for(let i = 0; i < notesList.length; i++) {
      statistics.total.value++;
      if(notesList[i].status === 'Completed') {
        statistics.completed.value++;
      }
      if(notesList[i].status === 'Not completed') {
        statistics.notCompleted.value++;
      }
    }
    return statistics;
  }
}
