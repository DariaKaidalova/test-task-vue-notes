import dbFile from '@/assets/db/db.json'
import EventService from '@/services/EventService'

export default {

  getNotes() {
    return new Promise((resolve) => {
      if (!this.getLocalStorageItems('notesList')) {
        this.setLocalStorageItems('notesList', dbFile.notesList);
      }
      let notesList = this.getLocalStorageItems('notesList');
      let statistics = EventService.calculateStatistics(notesList);
      resolve({notesList, statistics});
    });
  },

  setLocalStorageItems (itemsTitle, items) {
    window.localStorage.removeItem(itemsTitle);
    window.localStorage.setItem(itemsTitle, JSON.stringify(items));
  },

  getLocalStorageItems (itemsTitle) {
    return JSON.parse(window.localStorage.getItem(itemsTitle))
  },

  addNote(newNote) {
    return new Promise((resolve) => {
      let notesList = this.getLocalStorageItems('notesList');
      let updatedNotesList = EventService.addNote(newNote, notesList);
      this.setLocalStorageItems('notesList', updatedNotesList);
      let updatedStatistics = EventService.calculateStatistics(updatedNotesList);
      resolve({updatedNotesList, updatedStatistics});
    });
  },

  sortNotesList(sortColumn, isAsc) {
    return new Promise((resolve) => {
      let notesList = this.getLocalStorageItems('notesList');
      let sortedNotesList = EventService.sortNotesList(sortColumn, notesList, isAsc);
      resolve(sortedNotesList);
    });
  },

  deleteNotes(deleteNotesArray) {
    return new Promise((resolve) => {
      let notesList = this.getLocalStorageItems('notesList');
      let updatedNotesList = EventService.deleteNotes(deleteNotesArray, notesList);
      this.setLocalStorageItems('notesList', updatedNotesList);
      let updatedStatistics = EventService.calculateStatistics(updatedNotesList);
      resolve({updatedNotesList, updatedStatistics});
    });
  }
}
