# Test task

## Project structure

### The project consist of the following parts:
- 'project' folder with vue-cli

## About project

### How to start vue project
- Go into the 'project' folder 
- Install it using `npm install` command
- Start project using `npm run serve`

### Vue project structure
- Styles:
I used sass for styling. They are located in the 'src/assets/styles'.
- Components:
I created components in the folder 'src/components'.
- Services: 
I created mock rest service ('MockRestService.js' file) and event service ('EventService.js') in the 'services' folder.
- DB file:
DB file with initial mock data lies in the folder 'src/assets/db'. I used localStorage as a mock server. 